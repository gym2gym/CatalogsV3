Variaveis de ambiente a serem configuradas na fase de Deploy no Openshift:

MYSQL_USER

	exemplo: secret mysql -> database-user

MYSQL_PASSWORD

	exemplo: secret mysql -> database-password

MYSQL_DATABASE

	exemplo: secret mysql -> database-name

As variaveis abaixo ja estao presentes no Openshift

	MYSQL_SERVICE_HOST
	MYSQL_SERVICE_PORT
